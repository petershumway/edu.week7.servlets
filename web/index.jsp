<%--
  Created by IntelliJ IDEA.
  User: peter
  Date: 6/3/2020
  Time: 10:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>The Server</title>
  </head>
  <body>
  <h2>The server is up and running.</h2>
  <br>
  <p>The test newsletter sign-up form can be accessed <a href="${pageContext.request.contextPath}/signup.html">here</a>.</p>
  </body>
</html>
