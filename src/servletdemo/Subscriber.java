package servletdemo;

public class Subscriber {
    // Data member variables
    private String name;
    private String address1;
    private String address2;
    private String email;
    private int age;

    // Member methods

    // The constructer will fill the data by default to help avoid any errors of missing data
    Subscriber() {
        this.name = "Missing Name";
        this.address1 = "123 Missing St";
        this.address2 = "Nowhere, AZ 00000";
        this.email = "missing@gmail.com";
        this.age = 20;
    }

    // getter methods
    public String getName() {
        return this.name;
    }

    public String getAddress1() {
        return this.address1;
    }

    public String getAddress2() {
        return this.address2;
    }

    public String getEmail() {
        return this.email;
    }

    public int getAge() {
        return this.age;
    }

    // setter methods
    public void setName(String name) {
        this.name = name;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAge(int age) {
        this.age = age;
    }

    // prints the data out in html format
    public String toString() {
        return "Student Name: " + this.name + "<br>Address: " + this.address1 +
                this.address2 + "<br>Email: " + this.email + "<br>Age: " + this.age + "<br>";
    }

}
