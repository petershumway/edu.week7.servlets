package servletdemo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name = request.getParameter("name");
        String address1 = request.getParameter("address1");
        String address2 = request.getParameter("address2");
        String email = request.getParameter("email");
        String age = request.getParameter("age");

        // Save the info to a file.
        Subscriber newSubscriber = new Subscriber();
        newSubscriber.setName(name);
        newSubscriber.setAddress1(address1);
        newSubscriber.setAddress2(address2);

        // Validate correct email or fill in a placeholder for invalid email
        if (email.matches("^[\\w-_.+]*[\\w-_.]@([\\w]+\\.)+[\\w]+[\\w]$")) {
            newSubscriber.setEmail(email);
        }
        else {
            newSubscriber.setEmail("invalid.email@error.com");
        }
        // Validate correct age or fill in a placeholder for invalid age
        int verifyAge = Integer.parseInt(age);

        if (verifyAge > 0 && verifyAge < 108) {
            newSubscriber.setAge(verifyAge);
        }
        else {
            newSubscriber.setAge(13);
        }

        // confirmation screen in html format
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        out.println("<h1>Sign-up successful!</h1>");
        out.println("<p>You were signed up using the following information:</p>");
        out.println("<p>" + newSubscriber.toString() + "</p>");

        // return link
        out.println("<p> <a href=\"signup.html\">Return to Sign-up page</a>.</p>");
        out.println("</body><html>");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.println("<html><head></head><body>");
        out.println("<p>Did you mean to come to this page?</p>");
        out.println("<p> <a href=\"signup.html\">Return to Sign-up page</a>.</p>");
        out.println("</body><html>");
    }

}
